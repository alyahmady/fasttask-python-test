-- upgrade --
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(100) NOT NULL,
    "content" JSONB NOT NULL
);
CREATE TABLE IF NOT EXISTS "test_project_category" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "name" VARCHAR(128) NOT NULL,
    "is_active" BOOL NOT NULL  DEFAULT True
);
CREATE TABLE IF NOT EXISTS "test_project_member" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "username" VARCHAR(128) NOT NULL UNIQUE,
    "is_active" BOOL NOT NULL  DEFAULT True
);
CREATE INDEX IF NOT EXISTS "idx_test_projec_usernam_6acf7a" ON "test_project_member" ("username");
CREATE TABLE IF NOT EXISTS "test_project_document" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "subject" VARCHAR(128) NOT NULL,
    "content" TEXT NOT NULL,
    "is_active" BOOL NOT NULL  DEFAULT True,
    "author_id" INT REFERENCES "test_project_member" ("id") ON DELETE SET NULL,
    "category_id" INT NOT NULL REFERENCES "test_project_category" ("id") ON DELETE CASCADE
);
CREATE TABLE IF NOT EXISTS "test_project_category_permission" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "can_create" BOOL NOT NULL  DEFAULT False,
    "can_update" BOOL NOT NULL  DEFAULT False,
    "can_delete" BOOL NOT NULL  DEFAULT False,
    "can_read" BOOL NOT NULL  DEFAULT False,
    "is_active" BOOL NOT NULL  DEFAULT True,
    "category_id" INT NOT NULL REFERENCES "test_project_category" ("id") ON DELETE CASCADE,
    "member_id" INT NOT NULL REFERENCES "test_project_member" ("id") ON DELETE CASCADE,
    CONSTRAINT "uid_test_projec_member__5a998e" UNIQUE ("member_id", "category_id")
);
CREATE TABLE IF NOT EXISTS "test_project_document_permission" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "can_create" BOOL NOT NULL  DEFAULT False,
    "can_update" BOOL NOT NULL  DEFAULT False,
    "can_delete" BOOL NOT NULL  DEFAULT False,
    "can_read" BOOL NOT NULL  DEFAULT False,
    "is_active" BOOL NOT NULL  DEFAULT True,
    "document_id" INT NOT NULL REFERENCES "test_project_document" ("id") ON DELETE CASCADE,
    "member_id" INT NOT NULL REFERENCES "test_project_member" ("id") ON DELETE CASCADE,
    CONSTRAINT "uid_test_projec_member__821716" UNIQUE ("member_id", "document_id")
);
