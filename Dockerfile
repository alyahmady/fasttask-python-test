FROM python:3.9

# Set environment variables.
ENV PYTHONWRITEBYTECODE 1
ENV PYTHONBUFFERED 1

# Set working directory.
WORKDIR /code

# Copy dependencies.
COPY . .

# Install dependencies.
RUN pip install -r requirements.txt

EXPOSE 8000

ENTRYPOINT [ "bash", "/code/start.sh" ]
