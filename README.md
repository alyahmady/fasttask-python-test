## Installation (Docker Compose)

For first steps, please make a copy of `.env.sample` and rename it to `.env`
Also, please make a copy of `.env.db.sample` and rename it to `.env.db`.

```python
>>> sudo docker-compose build
>>> sudo docker-compose up -d

# To manage migrations of Tortoise, within the container:

>>> docker exec -it test_project

# Then these commands in container terminal:

>>> aerich init -t src.database.TORTOISE_ORM
>>> aerich init-db
>>> aerich migrate --name MIGRATION_COMMENT
>>> aerich upgrade
```

After these steps, app will be running on the localhost of the computer that is running docker, on port 80.

You can handle DNS or port forwarding on your host machine. Or, you can setup them on Nginx configuration.

Nginx conf is located in `nginx/cond.f/app.conf`.

---

## Notes

1- Postman collection is located in `docs` folder.

2- Honestly, it was my first experiment with Tortoise. It's a brilliant idea, but I think it's not stable yet. I faced many problems on it, mostly on Pydantic schema generation. That's what I draw back to writing Base models by by myself.

3- I decided to give all permissions of a document, to its author, because I thought it's rational. I didn't want to waste time by asking questions, because it's just a test. BTW, it's easily removable.

4- I decided to set an `is_active` field for all models and disable this field, instead of deleting records.

5- Also, all reads and updates will ignore deleted fields. Also, relations, for the parts that Cascade were necessary, will be deleted by their relation, as well.


# Document Permission System

We want to implement a system that manages the documents of a company.
In this system members can access or modify the documents based on their permissions.

## APIs

Members can do CRUD operations based on their permissions and documents using APIs.

## Member

Members should have `username` and an unique `id`.
**Default member should be `admin`.**

**Note**: We skip authorization in this system and only validate users by `username` that exists in the requets `headers`.

## Document

Documents should have `author`, `category`, `subject`, and `content`.

### Category

Categories should have only `name`.

## Permission

Permissions include `create`, `read`, `update` and `delete`.

Only `admin` member can set permission for other memebers to access or modify a specific document or the documents of a category.

## Category Permission

The `admin` can set permissions for a category and the permission will apply to all documents related to the category.

For example the member `John`, could `create` and `read` all documents in the category `Report`.

## Document Permission

Also admin can set permission a specific document in the category and the permission will only apply to that document.

For example the member `Alice` could `read` all documents in the category `Outcomes` but only could `update`
the document with id `3`.

## Dependencies

- Python 3.9 or later.
- FastAPI
- Tortoise-ORM.

## Initial Project Setup

1. Install [Poetry](https://python-poetry.org/)
2. Run `poetry install && poetry shell`


## Test Case Examples

### Set Category Permission

Request url: <http://localhost:8000/permission/create/>

Request data example:

```json
{
    "category_id": 1,
    "member_id": 2,
    "can_create": false,
    "can_read": true,
    "can_update": true,
    "can_delete": false
}
```

### Set Document Permission

Request url: <http://localhost:8000/permission/create/>

Request data example:

```json
{
    "document_id": 1,
    "member_id": 3,
    "can_create": false,
    "can_read": true,
    "can_update": true,
    "can_delete": false
}
```

## Get Documents

Request url: <http://localhost:8000/document/>

Response example:

```json
[
    {
        "id": 1,
        "category": {
            "id": 1,
            "name": "Report"
        },
        "author": {
            "id": 2,
            "username": "john"
        },
        "can_create": false,
        "can_read": true,
        "can_update": true,
        "can_delete": false
    },
    {
        "id": 2,
        "category": {
            "id": 1,
            "name": "Report"
        },
        "author": {
            "id": 2,
            "username": "alice"
        },
        "can_create": false,
        "can_read": true,
        "can_update": false,
        "can_delete": false
    }
]
```
