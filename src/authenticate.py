from fastapi import HTTPException

from src.database.models import Member
from src.settings import ADMIN_USERNAME


def is_member_admin(member: Member, raise_exc: bool = True):
    if member.username != ADMIN_USERNAME:
        if raise_exc:
            raise HTTPException(
                status_code=403, detail=f"Only admin members can create categories."
            )
        else:
            return False
    return True

    # TODO add authentication