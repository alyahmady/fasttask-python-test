from typing import List

from fastapi import APIRouter
from starlette.requests import Request
from tortoise.contrib.fastapi import HTTPNotFoundError

from src.authenticate import is_member_admin
from src.database.crud import db_create_category, db_get_category, db_update_category, db_delete_category
from src.database.models import Category
from src.database.schemas import CategoryBase, CategoryInBase, CategoryUpBase
from src.responses import SuccessResponse, ErrorResponse

router = APIRouter()

@router.post("/")
async def create_category(request: Request, category: CategoryInBase):
    is_member_admin(member=request.state.auth_user)

    category_obj: Category = await db_create_category(**category.dict(exclude_unset=True))
    category_data = CategoryBase.from_model(category_obj).dict()

    return SuccessResponse(status_code=201, message="Category created successfully.", content=category_data)


@router.get("/")
async def category_list():
    categories = await Category.filter(is_active=True)
    categories_data: List[CategoryBase] = [
        CategoryBase.from_model(category).dict() for category in categories
    ]

    return SuccessResponse(message="Categories retrieved successfully.", content=categories_data)


@router.get("/{category_id}", responses={404: {"model": HTTPNotFoundError}})
async def get_category(category_id: int):
    category = await db_get_category(id=category_id)
    if category:
        category_data = CategoryBase.from_model(category).dict()
        return SuccessResponse(message="Category retrieved successfully.", content=category_data)

    return ErrorResponse(status_code=404, message="Category not found")


@router.put("/{category_id}", responses={404: {"model": HTTPNotFoundError}})
async def update_category(request: Request, category_id: int, category: CategoryUpBase):
    is_member_admin(member=request.state.auth_user)

    category = await db_update_category(category_id=category_id, **category.dict(exclude_unset=True))
    if category:
        category_data = CategoryBase.from_model(category).dict()

        return SuccessResponse(message="Category updated successfully.", content=category_data)

    return ErrorResponse(status_code=404, message="Category not found")


@router.delete("/{category_id}", responses={404: {"model": HTTPNotFoundError}})
async def delete_category(request: Request, category_id: int):
    is_member_admin(member=request.state.auth_user)

    await db_delete_category(category_id=category_id)

    return SuccessResponse(message=f"Deleted category {category_id}.", status_code=204)
