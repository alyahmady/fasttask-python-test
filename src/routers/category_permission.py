from typing import List

from fastapi import APIRouter
from starlette.requests import Request
from tortoise.contrib.fastapi import HTTPNotFoundError

from src.authenticate import is_member_admin
from src.database.crud import db_create_category_permission, db_get_category_permission, db_update_category_permission, \
    db_delete_category_permission
from src.database.models import CategoryPermission
from src.database.schemas import CategoryPermissionBase, CategoryPermissionInBase, CategoryPermissionUpBase
from src.responses import SuccessResponse, ErrorResponse

router = APIRouter()


@router.post("/")
async def create_category_permission(request: Request, category_permission: CategoryPermissionInBase):
    is_member_admin(member=request.state.auth_user)

    category_permission_obj = await db_create_category_permission(**category_permission.dict(exclude_unset=True))
    category_permission_data = CategoryPermissionBase.from_model(category_permission_obj).dict()

    return SuccessResponse(status_code=201, message="Category permission created successfully.", content=category_permission_data)


@router.get("/")
async def category_permission_list(request: Request):
    is_member_admin(member=request.state.auth_user)

    category_permissions = await CategoryPermission.filter(is_active=True)
    category_permissions_data: List[CategoryPermissionBase] = [
        CategoryPermissionBase.from_model(category_permission).dict() for category_permission in category_permissions
    ]

    return SuccessResponse(message="Category permissions retrieved successfully.", content=category_permissions_data)


@router.get("/{category_permission_id}", responses={404: {"model": HTTPNotFoundError}})
async def get_category_permission(request: Request, category_permission_id: int):
    is_member_admin(member=request.state.auth_user)

    category_permission = await db_get_category_permission(id=category_permission_id)
    if category_permission:
        category_permission_data = CategoryPermissionBase.from_model(category_permission).dict()
        return SuccessResponse(message="Category permission retrieved successfully.", content=category_permission_data)

    return ErrorResponse(status_code=404, message="Category permission not found")


@router.put("/{category_permission_id}", responses={404: {"model": HTTPNotFoundError}})
async def update_category_permission(
        request: Request, category_permission_id: int, category_permission: CategoryPermissionUpBase
):
    is_member_admin(member=request.state.auth_user)

    category_permission = await db_update_category_permission(
        category_permission_id=category_permission_id, **category_permission.dict(exclude_unset=True)
    )
    if category_permission:
        category_permission_data = CategoryPermissionBase.from_model(category_permission).dict()
        return SuccessResponse(message="Category permission updated successfully.", content=category_permission_data)

    return ErrorResponse(status_code=404, message="Category permission not found")


@router.delete("/{category_permission_id}", responses={404: {"model": HTTPNotFoundError}})
async def delete_category_permission(request: Request, category_permission_id: int):
    is_member_admin(member=request.state.auth_user)

    await db_delete_category_permission(category_permission_id=category_permission_id)
    return SuccessResponse(message=f"Deleted category permission {category_permission_id}.", status_code=204)
