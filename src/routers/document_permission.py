from typing import List

from fastapi import APIRouter
from starlette.requests import Request
from tortoise.contrib.fastapi import HTTPNotFoundError

from src.authenticate import is_member_admin
from src.database.crud import db_create_document_permission, db_get_document_permission, db_update_document_permission, \
    db_delete_document_permission
from src.database.models import DocumentPermission
from src.database.schemas import DocumentPermissionBase, DocumentPermissionInBase, DocumentPermissionUpBase
from src.responses import SuccessResponse, ErrorResponse

router = APIRouter()


@router.post("/")
async def create_document_permission(request: Request, document_permission: DocumentPermissionInBase):
    is_member_admin(member=request.state.auth_user)

    document_permission_obj = await db_create_document_permission(**document_permission.dict(exclude_unset=True))
    document_permission_data = DocumentPermissionBase.from_model(document_permission_obj).dict()

    return SuccessResponse(status_code=201, message="Document permission created successfully.", content=document_permission_data)


@router.get("/", response_model=List[DocumentPermissionBase])
async def document_permission_list(request: Request):
    is_member_admin(member=request.state.auth_user)

    document_permissions = await DocumentPermission.filter(is_active=True)
    document_permissions_data: List[DocumentPermissionBase] = [
        DocumentPermissionBase.from_model(document_permission).dict() for document_permission in document_permissions
    ]

    return SuccessResponse(message="Document permissions retrieved successfully.", content=document_permissions_data)


@router.get(
    "/{document_permission_id}",
    response_model=DocumentPermissionBase,
    responses={404: {"model": HTTPNotFoundError}}
)
async def get_document_permission(request: Request, document_permission_id: int):
    is_member_admin(member=request.state.auth_user)

    document_permission = await db_get_document_permission(id=document_permission_id)
    if document_permission:
        document_permission_data = DocumentPermissionBase.from_model(document_permission).dict()
        return SuccessResponse(message="Document permission retrieved successfully.", content=document_permission_data)

    return ErrorResponse(status_code=404, message="Category permission not found")


@router.put("/{document_permission_id}", responses={404: {"model": HTTPNotFoundError}})
async def update_document_permission(
        request: Request, document_permission_id: int, document_permission: DocumentPermissionUpBase
):
    is_member_admin(member=request.state.auth_user)

    document_permission = await db_update_document_permission(
        document_permission_id=document_permission_id, **document_permission.dict(exclude_unset=True)
    )
    if document_permission:
        document_permission_data = DocumentPermissionBase.from_model(document_permission).dict()
        return SuccessResponse(message="Document permission updated successfully.", content=document_permission_data)

    return ErrorResponse(status_code=404, message="Document permission not found")


@router.delete("/{document_permission_id}", responses={404: {"model": HTTPNotFoundError}})
async def delete_document_permission(request: Request, document_permission_id: int):
    is_member_admin(member=request.state.auth_user)

    await db_delete_document_permission(document_permission_id=document_permission_id)
    return SuccessResponse(message=f"Deleted document permission {document_permission_id}.", status_code=204)
