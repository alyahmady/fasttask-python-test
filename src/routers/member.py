from typing import List

from fastapi import APIRouter
from starlette.requests import Request
from tortoise.contrib.fastapi import HTTPNotFoundError

from src.authenticate import is_member_admin
from src.database.crud import db_create_member, db_update_member, db_delete_member
from src.database.models import Member
from src.database.schemas import MemberBase, MemberInBase, MemberUpBase
from src.responses import SuccessResponse, ErrorResponse

router = APIRouter()

@router.post("/")
async def create_member(request: Request, member: MemberInBase):
    is_member_admin(member=request.state.auth_user)

    member_obj = await db_create_member(**member.dict(exclude_unset=True))
    member_data = MemberBase.from_model(member_obj).dict()

    return SuccessResponse(status_code=201, message="Member created successfully.", content=member_data)


@router.get("/")
async def member_list(request: Request):
    is_member_admin(member=request.state.auth_user)

    members = await Member.filter(is_active=True)
    members_data: List[MemberBase] = [MemberBase.from_model(document).dict() for document in members]

    return SuccessResponse(message="Members retrieved successfully.", content=members_data)


@router.get("/{member_id}", responses={404: {"model": HTTPNotFoundError}})
async def get_member(request: Request, member_id: int):
    is_member_admin(member=request.state.auth_user)

    member = await Member.filter(id=member_id, is_active=True).first()
    if member:
        member_data = MemberBase.from_model(member).dict()
        return SuccessResponse(message="Member retrieved successfully.", content=member_data)

    return ErrorResponse(status_code=404, message="Member not found")


@router.put("/{member_id}", responses={404: {"model": HTTPNotFoundError}})
async def update_member(request: Request, member_id: int, member: MemberUpBase):
    is_member_admin(member=request.state.auth_user)

    member = await db_update_member(member_id=member_id, **member.dict(exclude_unset=True))
    if member:
        member_data = MemberBase.from_model(member).dict()
        return SuccessResponse(message="Member updated successfully.", content=member_data)

    return ErrorResponse(status_code=404, message="Member not found")


@router.delete("/{member_id}", responses={404: {"model": HTTPNotFoundError}})
async def delete_member(request: Request, member_id: int):
    is_member_admin(member=request.state.auth_user)

    await db_delete_member(member_id=member_id)
    return SuccessResponse(message=f"Deleted member {member_id}.", status_code=204)
