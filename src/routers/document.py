from fastapi import APIRouter, HTTPException
from starlette.requests import Request
from tortoise.contrib.fastapi import HTTPNotFoundError
from tortoise.query_utils import Q

from src.authenticate import is_member_admin
from src.database.crud import db_create_document, db_get_document, db_update_document, \
    db_delete_document, db_get_document_permission
from src.database.crud.category_permission import db_get_category_permission
from src.database.models import Document, DocumentPermission
from src.database.schemas import DocumentBase, DocumentInBase, DocumentUpBase
from src.responses import SuccessResponse, ErrorResponse

router = APIRouter()

@router.post("/")
async def create_document(request: Request, document: DocumentInBase):
    auth_user_id = request.state.auth_user_id

    category_id = getattr(document, 'category_id', None)
    if not category_id:
        raise HTTPException(status_code=400, detail=f"Invalid input. `category_id` must be set for document.")

    has_permission = is_member_admin(member=request.state.auth_user, raise_exc=False)
    if not has_permission:
        permission = await db_get_category_permission(member_id=auth_user_id, category_id=category_id)
        has_permission = bool(permission and permission.can_create is True)

    if has_permission:
        document_obj = await db_create_document(
            author_id=auth_user_id, **document.dict(exclude_unset=True)
        )
        document_item = await DocumentBase.from_model(document_obj)
        document_data = document_item.dict()

        return SuccessResponse(status_code=201, message="Document created successfully.", content=document_data)

    raise HTTPException(
        status_code=403, detail=f"Member {auth_user_id} do not have access to create document in category {category_id}"
    )


@router.get("/")
async def document_list(request: Request):
    auth_user_id = request.state.auth_user_id

    authored_conditions = Q(Q(author_id=auth_user_id), Q(is_active=True))
    granted_direct_conditions = Q(
        Q(permissions__member_id=auth_user_id), Q(permissions__is_active=True),
        Q(permissions__can_read=True), Q(is_active=True)
    )
    granted_category_conditions = Q(
        Q(category__permissions__member_id=auth_user_id), Q(category__permissions__is_active=True),
        Q(category__permissions__can_read=True), Q(is_active=True)
    )

    if is_member_admin(request.state.auth_user, raise_exc=False):
        documents = await Document.filter(is_active=True).all()
    else:
        documents = await Document.filter(Q(
            authored_conditions, granted_direct_conditions, granted_category_conditions, join_type="OR"
        )).distinct().all()

    documents_data = []
    for document in documents:
        document_item = await DocumentBase.from_model(document)
        document_data = document_item.dict()
        documents_data.append(document_data)

    return SuccessResponse(message="Documents retrieved successfully.", content=documents_data)


@router.get("/{document_id}", responses={404: {"model": HTTPNotFoundError}})
async def get_document(request: Request, document_id: int):
    auth_user_id = request.state.auth_user_id

    has_permission = is_member_admin(member=request.state.auth_user, raise_exc=False)
    if not has_permission:
        permission: DocumentPermission = await db_get_document_permission(
            member_id=auth_user_id, document_id=document_id
        )
        has_permission = bool(permission and permission.can_read is True)

    if has_permission:
        document = await db_get_document(id=document_id)
        if document:
            document_item = await DocumentBase.from_model(document)
            document_data = document_item.dict()
            return SuccessResponse(message="Document retrieved successfully.", content=document_data)

    return ErrorResponse(status_code=404, message="Document not found")
    # raise HTTPException(
    #     status_code=403, detail=f"Member {auth_user_id} do not have access to read document {document_id}"
    # )


@router.put("/{document_id}", responses={404: {"model": HTTPNotFoundError}})
async def update_document(request: Request, document_id: int, document: DocumentUpBase):
    auth_user_id = request.state.auth_user_id

    has_permission = is_member_admin(member=request.state.auth_user, raise_exc=False)

    if not has_permission:
        permission = await db_get_document_permission(member_id=auth_user_id, document_id=document_id)
        has_permission = bool(permission and permission.can_update is True)

    if has_permission:
        document = await db_update_document(document_id=document_id, **document.dict(exclude_unset=True))
        if document:
            document_item = await DocumentBase.from_model(document)
            document_data = document_item.dict()
            return SuccessResponse(message="Document updated successfully.", content=document_data)

    return ErrorResponse(status_code=404, message="Document not found")
    # raise HTTPException(
    #     status_code=403, detail=f"Member {auth_user_id} do not have access to update document {document_id}"
    # )


@router.delete("/{document_id}", responses={404: {"model": HTTPNotFoundError}})
async def delete_document(request: Request, document_id: int):
    auth_user_id = request.state.auth_user_id

    has_permission = is_member_admin(member=request.state.auth_user, raise_exc=False)
    if not has_permission:
        permission = await db_get_document_permission(member_id=auth_user_id, document_id=document_id)
        has_permission = bool(permission and permission.can_delete is True)

    if has_permission:
        await db_delete_document(document_id=document_id)
        return SuccessResponse(message=f"Deleted document {document_id}.", status_code=204)

    return ErrorResponse(status_code=404, message="Document not found")
    # raise HTTPException(
    #     status_code=403, detail=f"Member {auth_user_id} do not have access to delete document {document_id}"
    # )