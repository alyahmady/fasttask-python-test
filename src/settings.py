import os

DEBUG = os.getenv("DEBUG", "true")
DEBUG = True if DEBUG.lower() == "true" else False

DB_HOST = os.getenv("DB_HOST", "localhost")
DB_NAME = os.getenv("DB_NAME", "postgres")
DB_USER = os.getenv("DB_USER", "postgres")
DB_PASSWORD = os.getenv("DB_PASSWORD", "postgres")
DB_PORT = os.getenv("DB_PORT", "5432")

DB_CONN_STRING = f"postgres://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

TABLE_PREFIX = os.getenv("TABLE_PREFIX", "test_project")

USERNAME_HTTP_HEADER_KEY = os.getenv("USERNAME_HTTP_HEADER_KEY", "Auth-User")

ADMIN_USERNAME = os.getenv("ADMIN_USERNAME", "admin")