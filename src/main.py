import traceback

from fastapi import FastAPI, HTTPException
from starlette.requests import Request
from starlette.responses import JSONResponse
from tortoise.contrib.fastapi import register_tortoise
from tortoise.exceptions import BaseORMException

from src.middlewares import UserAuth
from src.routers import document, category, member, document_permission, category_permission
from src.settings import DB_CONN_STRING, DEBUG

app = FastAPI()
app.include_router(document.router, prefix="/document")
app.include_router(category.router, prefix="/category")
app.include_router(member.router, prefix="/member")
app.include_router(document_permission.router, prefix="/permission/document")
app.include_router(category_permission.router, prefix="/permission/category")

app.add_middleware(UserAuth)

register_tortoise(
    app=app,
    db_url=DB_CONN_STRING,
    modules={"models": ["src.database.models"]},
    generate_schemas=True,
    add_exception_handlers=True,
)

@app.exception_handler(HTTPException)
async def http_exception_handler(request: Request, exc: HTTPException):
    return JSONResponse(
        status_code=exc.status_code, content={"status": False, "message": exc.detail},
    )


@app.exception_handler(ValueError)
async def value_exception_handler(request: Request, exc: ValueError):
    message = {"status": False}
    if DEBUG:
        message.update({"message": traceback.format_exc(), "exception": str(exc)})
    return JSONResponse(
        status_code=400, content=message,
    )


@app.exception_handler(BaseORMException)
async def orm_exception_handler(request: Request, exc: ValueError):
    message = {"status": False}
    if DEBUG:
        message.update({"message": traceback.format_exc(), "exception": str(exc)})
    return JSONResponse(
        status_code=400, content=message,
    )