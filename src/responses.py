import typing

from pydantic import BaseModel
from starlette.responses import JSONResponse

from src.helpers import json_dump


RESPONSE_SCHEMA = {
    'status_code': None,
    'status': None,
    'data': None,
    'message': None
}

RESPONSE_SCHEMA_WITHOUT_DATA = {
    'status_code': None,
    'status': None,
    'message': None
}


class CustomResponse(JSONResponse):
    def __init__(self, message: typing.Optional[str] = None, **kwargs) -> None:
        self.message = message
        super(CustomResponse, self).__init__(**kwargs)


class SuccessResponse(CustomResponse):
    def render(self, content: typing.Optional[typing.Any] = None) -> bytes:
        response = RESPONSE_SCHEMA.copy()
        response.update({
            "message": self.message,
            "status_code": self.status_code or 200,
            "status": True,
        })
        if content:
            response.update({"data": content})

        return json_dump(data=response)


class ErrorResponse(CustomResponse):
    def render(self, content: typing.Any) -> bytes:
        response = RESPONSE_SCHEMA_WITHOUT_DATA.copy()
        response.update({
            "status": False,
            "message": self.message,
            "status_code": self.status_code,
        })

        return json_dump(data=response)


class StatusMessage(BaseModel):
    message: str
    code: str