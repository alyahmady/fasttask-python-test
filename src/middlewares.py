from fastapi import HTTPException
from starlette import status
from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint
from starlette.requests import Request
from starlette.responses import Response

from src.database.crud import db_get_member
from src.settings import USERNAME_HTTP_HEADER_KEY


class UserAuth(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next: RequestResponseEndpoint) -> Response:
        try:
            auth_user_name = request.headers[USERNAME_HTTP_HEADER_KEY]
            request.state.auth_user = await db_get_member(username=auth_user_name)
            request.state.auth_user_id = request.state.auth_user.id
            assert None not in (request.state.auth_user, request.state.auth_user_id)
        except (KeyError, AssertionError, AttributeError):
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail=f"Authentication header is not valid.")

        return await call_next(request)