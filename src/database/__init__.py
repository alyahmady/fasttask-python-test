from src.settings import DB_CONN_STRING

TORTOISE_ORM = {
    "connections": {"default": DB_CONN_STRING},
    "apps": {
        "models": {
            "models": [
                "aerich.models",
                "src.database.models.category",
                "src.database.models.document",
                "src.database.models.member",
                "src.database.models.permission",
            ],
            "default_connection": "default",
        },
    },
}