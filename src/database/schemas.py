from typing import Optional

from pydantic import BaseModel

from src.database.crud import db_get_category, db_get_member
from src.database.models import Member, Category, Document, DocumentPermission, CategoryPermission


class MemberInBase(BaseModel):
    username: str


class MemberBase(MemberInBase):
    id: int
    username: str

    @classmethod
    def from_model(cls, db_item: Member):
        return cls(id=db_item.id, username=db_item.username)


class MemberUpBase(MemberBase):
    id: Optional[int] = None
    username: Optional[str] = None


class CategoryInBase(BaseModel):
    name: str


class CategoryBase(CategoryInBase):
    id: int
    name: str

    @classmethod
    def from_model(cls, db_item: Category):
        return cls(id=db_item.id, name=db_item.name)


class CategoryUpBase(CategoryBase):
    id: Optional[int] = None
    name: Optional[str] = None


class DocumentInBase(BaseModel):
    category_id: int
    subject: str
    content: str


class DocumentBase(BaseModel):
    id: int
    author: dict
    category: dict
    subject: str
    content: str

    @classmethod
    async def from_model(cls, db_item: Document):
        category = await db_item.category.first()
        author = await db_item.author.first()

        return cls(
            id=db_item.id,
            author={
                "id": author.id,
                "username": author.username
            },
            category={
                "id": category.id,
                "name": category.name
            },
            subject=db_item.subject,
            content=db_item.content
        )


class DocumentUpBase(DocumentBase):
    id: Optional[int] = None
    author: Optional[dict] = None
    category: Optional[dict] = None
    subject: Optional[str] = None
    content: Optional[str] = None


class DocumentPermissionInBase(BaseModel):
    member_id: int
    document_id: int

    can_create: bool
    can_update: bool
    can_delete: bool
    can_read: bool


class DocumentPermissionBase(BaseModel):
    id: int

    member_id: int
    document_id: int

    can_create: bool
    can_update: bool
    can_delete: bool
    can_read: bool

    @classmethod
    def from_model(cls, db_item: DocumentPermission):
        return cls(
            id=db_item.id,
            member_id=db_item.member_id,
            document_id=db_item.document_id,
            can_create=db_item.can_create,
            can_update=db_item.can_update,
            can_delete=db_item.can_delete,
            can_read=db_item.can_read
        )


class DocumentPermissionUpBase(DocumentPermissionBase):
    id: Optional[int] = None

    member_id: Optional[int] = None
    document_id: Optional[int] = None

    can_create: Optional[bool] = None
    can_update: Optional[bool] = None
    can_delete: Optional[bool] = None
    can_read: Optional[bool] = None


class CategoryPermissionInBase(BaseModel):
    member_id: int
    category_id: int

    can_create: bool
    can_update: bool
    can_delete: bool
    can_read: bool


class CategoryPermissionBase(BaseModel):
    id: int

    member_id: int
    category_id: int

    can_create: bool
    can_update: bool
    can_delete: bool
    can_read: bool

    @classmethod
    def from_model(cls, db_item: CategoryPermission):
        return cls(
            id=db_item.id,
            member_id=db_item.member_id,
            category_id=db_item.category_id,
            can_create=db_item.can_create,
            can_update=db_item.can_update,
            can_delete=db_item.can_delete,
            can_read=db_item.can_read
        )


class CategoryPermissionUpBase(CategoryPermissionBase):
    id: Optional[int] = None

    member_id: Optional[int] = None
    category_id: Optional[int] = None

    can_create: Optional[bool] = None
    can_update: Optional[bool] = None
    can_delete: Optional[bool] = None
    can_read: Optional[bool] = None