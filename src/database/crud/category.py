from typing import Optional, List

from src.database.models import Document, Category, CategoryPermission
from src.helpers import bool_or_false


async def db_get_category(**kwargs) -> Optional[Category]:
    filter_fields = ("name", "id")
    filter_kwargs = {key: kwargs[key] for key in kwargs if key in filter_fields}
    if not filter_kwargs:
        raise ValueError("Invalid arguments.")

    return await Category.filter(**filter_kwargs, is_active=True).first()


async def db_get_category_documents_by_permission(member_id: int, **kwargs) -> List[Document]:
    filter_fields = ("can_create", "can_update", "can_delete", "can_read")
    filter_kwargs = {f"category__permissions__{key}": bool_or_false(kwargs[key]) for key in kwargs if key in filter_fields}
    if not filter_kwargs:
        raise ValueError("Invalid arguments.")

    return await Document.filter(category__permissions__member_id=member_id, is_active=True,
                                 category__permissions__is_active=True, **filter_kwargs)


async def db_search_category_by_name(name: str) -> List[Category]:
    return await Category.filter(name__search=name, is_active=True).all()


async def db_create_category(name: str) -> Category:
    return await Category.create(name=name, is_active=True)


async def db_update_category(category_id: int, **kwargs) -> Optional[Category]:
    valid_update_fields = ('name',)
    update_kwargs = {key: kwargs[key] for key in kwargs if key in valid_update_fields}
    if update_kwargs:
        await Category.filter(id=category_id, is_active=True).update(**update_kwargs)

    return await Category.filter(id=category_id, is_active=True).first()


async def db_delete_category(category_id: int):
    await Category.filter(id=category_id, is_active=True).update(is_active=False)

    # Disable all related documents
    await Document.filter(category_id=category_id, is_active=True).update(is_active=False)

    # Disable all assigned permissions
    await CategoryPermission.filter(category_id=category_id, is_active=True).update(is_active=False)