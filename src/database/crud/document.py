from typing import Optional, List

from src.database.models import Document, DocumentPermission
from src.helpers import bool_or_false


async def db_get_documents_by_author(author_id: int) -> List[Document]:
    return await Document.filter(author_id=author_id, is_active=True).all()


async def db_get_document(**kwargs) -> Optional[Document]:
    filter_fields = ("subject", "id")
    filter_kwargs = {key: kwargs[key] for key in kwargs if key in filter_fields}
    if not filter_kwargs:
        raise ValueError("Invalid arguments.")

    return await Document.filter(**filter_kwargs, is_active=True).first()


async def db_get_documents_by_permission(member_id: int, **kwargs) -> List[Document]:
    filter_fields = ("can_create", "can_update", "can_delete", "can_read")
    filter_kwargs = {f"permissions__{key}": bool_or_false(kwargs[key]) for key in kwargs if key in filter_fields}
    if not filter_kwargs:
        raise ValueError("Invalid arguments.")

    return await Document.filter(permissions__member_id=member_id, is_active=True,
                                 permissions__is_active=True, **filter_kwargs)


async def db_search_document_by_subject(subject: str) -> List[Document]:
    return await Document.filter(subject__search=subject, is_active=True).all()


async def db_create_document(subject: str, content: str, author_id: int, category_id: int) -> Document:
    document = await Document.create(subject=subject, content=content, author_id=author_id,
                                     category_id=category_id, is_active=True)

    # Set all permissions on this document for the author
    permission, _ = await DocumentPermission.get_or_create(member_id=author_id, document_id=document.id)
    update_kwargs = {f"can_{operation}": True for operation in ("create", "read", "update", "delete")}
    update_kwargs["is_active"] = True
    await DocumentPermission.filter(id=permission.id).update(**update_kwargs)

    return document


async def db_update_document(document_id: int, **kwargs) -> Optional[Document]:
    valid_update_fields = ('subject', 'content', 'author_id', 'category_id')
    update_kwargs = {key: kwargs[key] for key in kwargs if key in valid_update_fields}
    if update_kwargs:
        await Document.filter(id=document_id, is_active=True).update(**update_kwargs)

    return await Document.filter(id=document_id, is_active=True).first()


async def db_delete_document(document_id: int):
    await Document.filter(id=document_id, is_active=True).update(is_active=False)

    # Disable all permissions
    await DocumentPermission.filter(document_id=document_id, is_active=True).update(is_active=False)
