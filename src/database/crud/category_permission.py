from typing import Optional

from src.database.models import CategoryPermission


async def db_get_category_permission(**kwargs) -> Optional[CategoryPermission]:
    filter_fields = ("member_id", "category_id", "id")
    filter_kwargs = {key: kwargs[key] for key in kwargs if key in filter_fields}
    if not filter_kwargs:
        raise ValueError("Invalid arguments.")
    if "id" not in filter_kwargs:
        if "member_id" not in filter_kwargs or "category_id" not in filter_kwargs:
            raise ValueError("Invalid arguments.")

    return await CategoryPermission.filter(**filter_kwargs, is_active=True).first()


async def db_create_category_permission(
        member_id: int, category_id: int, can_create: bool,
        can_update: bool, can_delete: bool, can_read: bool
) -> CategoryPermission:
    return await CategoryPermission.create(
        member_id=member_id, category_id=category_id, can_create=can_create,
        can_update=can_update, can_delete=can_delete, can_read=can_read, is_active=True
    )


async def db_update_category_permission(category_permission_id: int, **kwargs) -> Optional[CategoryPermission]:
    valid_update_fields = ('can_delete', 'can_update', 'can_create', 'can_read',)
    update_kwargs = {key: kwargs[key] for key in kwargs if key in valid_update_fields}
    if update_kwargs:
        await CategoryPermission.filter(id=category_permission_id, is_active=True).update(**update_kwargs)

    return await CategoryPermission.filter(id=category_permission_id, is_active=True).first()


async def db_delete_category_permission(category_permission_id: int):
    await CategoryPermission.filter(id=category_permission_id, is_active=True).update(is_active=False)