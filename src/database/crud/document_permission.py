from typing import Optional

from src.database.models import DocumentPermission, Document, CategoryPermission


async def db_get_document_permission(**kwargs) -> Optional[DocumentPermission]:
    filter_fields = ("member_id", "document_id", "id")
    filter_kwargs = {key: kwargs[key] for key in kwargs if key in filter_fields}
    if not filter_kwargs:
        raise ValueError("Invalid arguments.")
    if "id" not in filter_kwargs:
        if "member_id" not in filter_kwargs or "document_id" not in filter_kwargs:
            raise ValueError("Invalid arguments.")

    permission = await DocumentPermission.filter(**filter_kwargs, is_active=True).first()
    if (not permission) and ("document_id" in filter_kwargs):
        document = await Document.filter(id=filter_kwargs["document_id"], is_active=True).first()
        permission = await CategoryPermission.filter(
            member_id=filter_kwargs["member_id"], category_id=document.category_id, is_active=True
        ).first()

    return permission


async def db_create_document_permission(
        member_id: int, document_id: int, can_create: bool, can_update: bool, can_delete: bool, can_read: bool
) -> DocumentPermission:
    return await DocumentPermission.create(
        member_id=member_id, document_id=document_id, is_active=True, can_delete=can_delete,
        can_create=can_create, can_update=can_update, can_read=can_read
    )


async def db_update_document_permission(document_permission_id: int, **kwargs) -> Optional[DocumentPermission]:
    valid_update_fields = ('can_delete', 'can_update', 'can_create', 'can_read',)
    update_kwargs = {key: kwargs[key] for key in kwargs if key in valid_update_fields}
    if update_kwargs:
        await DocumentPermission.filter(id=document_permission_id, is_active=True).update(**update_kwargs)

    return await DocumentPermission.filter(id=document_permission_id, is_active=True).first()


async def db_delete_document_permission(document_permission_id: int):
    await DocumentPermission.filter(id=document_permission_id, is_active=True).update(is_active=False)
