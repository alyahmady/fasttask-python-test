from typing import Optional

from src.database.models import Member, Document, DocumentPermission, CategoryPermission
from src.settings import ADMIN_USERNAME


async def db_get_member(**kwargs) -> Optional[Member]:
    filter_fields = ("username", "id")
    filter_kwargs = {key: kwargs[key] for key in kwargs if key in filter_fields}
    if not filter_kwargs:
        raise ValueError("Invalid arguments.")

    return await Member.filter(**filter_kwargs, is_active=True).first()

async def db_create_member(username: str) -> Member:
    return await Member.create(username=username, is_active=True)


async def db_update_member(member_id: int, **kwargs) -> Optional[Member]:
    valid_update_fields = ('username',)
    update_kwargs = {key: kwargs[key] for key in kwargs if key in valid_update_fields}
    if update_kwargs:
        await Member.filter(id=member_id, is_active=True).update(**update_kwargs)

    return await Member.filter(id=member_id, is_active=True).first()


async def db_delete_member(member_id: int):
    await Member.filter(id=member_id, is_active=True).update(is_active=False)

    # Set author as admin, for all related documents
    admin_member = await Member.filter(username=ADMIN_USERNAME, is_active=True).first()
    await Document.filter(author_id=member_id, is_active=True).update(author=admin_member)

    # Disable all permissions
    await DocumentPermission.filter(member_id=member_id, is_active=True).update(is_active=False)
    await CategoryPermission.filter(member_id=member_id, is_active=True).update(is_active=False)