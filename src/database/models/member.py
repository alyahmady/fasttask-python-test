from tortoise import Model, fields

from src.settings import TABLE_PREFIX


class Member(Model):
    id = fields.IntField(pk=True)
    username = fields.CharField(max_length=128, unique=True, index=True)

    is_active = fields.BooleanField(default=True)

    class Meta:
        table = f"{TABLE_PREFIX}_member"
        ordering = ["-is_active"]

    class PydanticMeta:
        pass
