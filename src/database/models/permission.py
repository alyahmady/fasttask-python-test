from tortoise import fields, Model

from src.settings import TABLE_PREFIX


class PermissionMixin():
    id = fields.IntField(pk=True)

    can_create = fields.BooleanField(default=False)
    can_update = fields.BooleanField(default=False)
    can_delete = fields.BooleanField(default=False)
    can_read = fields.BooleanField(default=False)


class CategoryPermission(PermissionMixin, Model):
    member = fields.ForeignKeyField('models.Member', related_name='category_permissions')
    category = fields.ForeignKeyField('models.Category', related_name='permissions')

    is_active = fields.BooleanField(default=True)

    class Meta:
        table = f"{TABLE_PREFIX}_category_permission"
        ordering = ["-is_active"]
        unique_together = (("member_id", "category_id"),)

    class PydanticMeta:
        pass


class DocumentPermission(PermissionMixin, Model):
    member = fields.ForeignKeyField('models.Member', related_name='document_permissions')
    document = fields.ForeignKeyField('models.Document', related_name='permissions')

    is_active = fields.BooleanField(default=True)

    class Meta:
        table = f"{TABLE_PREFIX}_document_permission"
        ordering = ["-is_active"]
        unique_together = (("member_id", "document_id"),)

    class PydanticMeta:
        pass

