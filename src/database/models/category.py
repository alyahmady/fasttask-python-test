from tortoise import Model, fields

from src.settings import TABLE_PREFIX


class Category(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=128)

    is_active = fields.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        table = f"{TABLE_PREFIX}_category"
        ordering = ["name", "-is_active"]

    class PydanticMeta:
        pass