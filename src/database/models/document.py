from tortoise import Model, fields
from tortoise.fields import SET_NULL

from src.settings import TABLE_PREFIX


class Document(Model):
    id = fields.IntField(pk=True)
    author = fields.ForeignKeyField('models.Member', related_name='documents', on_delete=SET_NULL, null=True)
    category = fields.ForeignKeyField('models.Category', related_name='documents')
    subject = fields.CharField(max_length=128)
    content = fields.TextField()

    is_active = fields.BooleanField(default=True)

    def __str__(self):
        return self.subject

    class Meta:
        table = f"{TABLE_PREFIX}_document"
        ordering = ["subject", "-is_active"]

    class PydanticMeta:
        pass