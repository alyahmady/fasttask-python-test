import logging
import os
import asyncio

from tenacity import after_log, before_log, retry, stop_after_attempt, wait_fixed
from tortoise import Tortoise, BaseDBAsyncClient

DB_HOST = os.getenv("DB_HOST", "localhost")
DB_NAME = os.getenv("DB_NAME", "postgres")
DB_USER = os.getenv("DB_USER", "postgres")
DB_PASSWORD = os.getenv("DB_PASSWORD", "postgres")
DB_PORT = os.getenv("DB_PORT", "5432")

DB_CONN_STRING = f"postgres://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

ADMIN_USERNAME = os.getenv("ADMIN_USERNAME", "admin")

TABLE_PREFIX = os.getenv("TABLE_PREFIX", "test_project")
MEMBER_TABLE_NAME = f"{TABLE_PREFIX}_member"

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

@retry(
    stop=stop_after_attempt(5),
    wait=wait_fixed(5),
    before=before_log(logger, logging.INFO),
    after=after_log(logger, logging.WARN),
)
async def start_connection_test() -> None:
    try:
        await Tortoise.init(db_url=DB_CONN_STRING, modules={'models': []})
        TortoiseConn: BaseDBAsyncClient = Tortoise.get_connection("default")
        await TortoiseConn.execute_query("SELECT 1")
    except Exception as e:
        raise e

    # Creating default admin user
    try:
        await TortoiseConn.execute_query_dict(
            f"INSERT INTO {MEMBER_TABLE_NAME} (username, is_active) VALUES ('{ADMIN_USERNAME}', true) "
            f"ON CONFLICT (username) DO NOTHING;"
        )
    except:
        pass


if __name__ == "__main__":
    asyncio.run(start_connection_test())