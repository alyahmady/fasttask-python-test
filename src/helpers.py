import datetime
import decimal
import json
import uuid


def bool_or_false(value):
    try:
        value = bool(value)
    except:
        value = None
    return value


class CustomJSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, uuid.UUID):
            return str(o)
        if isinstance(o, decimal.Decimal):
            return str(o)
        if isinstance(o, datetime.date) or isinstance(o, datetime.datetime):
            return o.isoformat()
        return super(CustomJSONEncoder, self).default(o)


def json_dump(data):
    return json.dumps(
        data,
        ensure_ascii=False,
        allow_nan=False,
        indent=None,
        separators=(",", ":"),
        cls=CustomJSONEncoder
    ).encode("utf-8")