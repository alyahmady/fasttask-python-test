#! /usr/bin/env bash

python ./src/database/connection.py

gunicorn src.main:app --workers 2 --worker-class uvicorn.workers.UvicornWorker -b 0.0.0.0:8000